const express = require('express');
const app = express();
const port = 3002;
const R = require('ramda');
const cors = require('cors');
const mockData = require('./data.json');
const mockInspirationData = require('./inspiration_data.json');
const mockDealerData = require('./dealer_data.json');
const mockColourPickerData = require('./colourpicker_data.json');

app.use(cors({ origin: true, credentials: true }));
app.get('/', (req, res) => res.send('Please specify a data file'));
app.get('/api/v1/search/dealer/query', function(req, res) {
    res.json(mockDealerData);
});
app.get('/api/v1/search/query', function(req, res) {
    const documentTypes = req.query['documentTypes'];
    if(documentTypes === 'Colours') return res.json(mockColourPickerData);
    documentTypes === 'Product' ? res.json(mockData) : res.json(mockInspirationData);
});
app.get('_OLD/api/v1/search/product/query', function(req, res) {
  const DEFAULT_PAGE_SIZE = 8;

  const keyMap = {
    productType: 'type',
  };
  const reverseKeyMap = {
    type: 'productType',
  };

  const queryKeys = Object.keys(req.query)
    .filter(q => q !== 'page' && q !== 'take')
    .map(key => (keyMap[key] ? keyMap[key] : key));

  const response =
    queryKeys.length > 0
      ? queryKeys.map(k =>
          mockData.data.products.map(d =>
            reverseKeyMap[k]
              ? d[k] == req.query[reverseKeyMap[k]] && d
              : d[k] == req.query[k] && d,
          ),
        )
      : mockData.data.products;

  const responseFiltered = R.flatten(response).filter(x => x);
  const currentPage = R.contains('page', Object.keys(req.query))
    ? parseInt(req.query['page'])
    : 0;
  const takeExists = R.contains('take', Object.keys(req.query));
  const pageSize = takeExists ? parseInt(req.query['take']) : DEFAULT_PAGE_SIZE;

  const paged = R.compose(
    R.take(pageSize),
    R.drop(currentPage * pageSize),
  );
  const actualRes = paged(responseFiltered);
  const mockDataWithTotalProducts = {
    ...mockData.data,
    totalProducts: responseFiltered.length,
  };
  const finalRes = {
    data: { ...mockDataWithTotalProducts, products: actualRes },
  };

  res.json(finalRes);
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
